import express from 'express'
import cors from 'cors'

const app = express();
app.use(cors());
app.get('/task2a', (req, res) => {
  try {
    const a = parseInt(req.query.a) != null && typeof req.query.a != 'undefined' && req.query.a != '' ? parseInt(req.query.a) : 0;
    const b = parseInt(req.query.b) != null && typeof req.query.b != 'undefined' && req.query.b != '' ? parseInt(req.query.b) : 0;
    return res.json(a + b);
  } catch (error) {
    console.log(error);
    res.json({ error });
  }
});

app.get('/task2b', (req, res) => {
  try {
    let canSend = true;
    console.log(req.query.fullname);
    const arFullname = typeof req.query.fullname != 'undefined' && req.query.fullname.length != 0 ? req.query.fullname.replace(/\s{2,}/g, ' ').toUpperCase().trim().split(' ') : [];
    const reNum = /(\d+)|[[,\],\,^,$,.,|,?,*,+,(,),_,/,>,<,\,]/g;
    arFullname.forEach(function (p, i) {
      if (reNum.test(p)) canSend = false;
    });

    if (arFullname.length > 3 || arFullname.length == 0 || !canSend) return res.send('Invalid fullname');
    let fullname = arFullname[arFullname.length - 1][0] + arFullname[arFullname.length - 1].substr(1, arFullname[arFullname.length - 1].length).toLowerCase();
    fullname += typeof arFullname[arFullname.length - 3] != 'undefined' ? ' ' + arFullname[arFullname.length - 3][0] + '.' : '';
    fullname += typeof arFullname[arFullname.length - 2] != 'undefined' ? ' ' + arFullname[arFullname.length - 2][0] + '.' : '';
    return res.send(fullname.toString());
  } catch (error) {
    console.log(error);
    res.json({ error });
  }
});

app.get('/task2b-2', (req, res) => {
  try {
    const input = typeof req.query.fullname != 'undefined' ? req.query.fullname : '';
    const re = /(\w+)\s(\w+)/, re2 = /($\n, )/;
    const output = input.length == 0 ? 'Undefined fullname' : input.replace(re, '$3, $2, $1').replace(re2, '');
    return res.json(output);
  } catch (error) {
    console.log(error);
    res.json({ error });
  }
});

app.get('/task2c', (req, res) => {
  try {
    const input = typeof req.query.username != 'undefined' ? req.query.username.trim().split('/') : [];
    let out = input.length > 0 ? '@' + input[input.length - 1].split('?')[0] : 'Invalid username';
    out = out.split('@').length > 2 && out != 'Invalid username' ? '@' + out.split('a')[out.split('a').length - 1] : out;
    out = out.replace(/^@*/, '@');
    return res.send(out);
  } catch (error) {
    console.log(error);
    res.json({ error });
  }
});

app.listen(3000, function () {
  console.log('App listening on port 3000...');
});